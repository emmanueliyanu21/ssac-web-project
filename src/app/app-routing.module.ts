import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { LeadershipComponent } from './components/pages/leadership/leadership.component';
import { CareerComponent } from './components/pages/career/career.component';
import { IndustryComponent } from './components/pages/industry/industry.component'

const routes: Routes = [
  {   path: '', 
      component: HomeComponent 
  },
  {
      path: 'contact',
      component: ContactComponent
  },
  {
      path: 'leadership',
      component: LeadershipComponent
  },
  {
      path: 'career',
      component: CareerComponent
  },
  {
      path: 'industry',
      component: IndustryComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
